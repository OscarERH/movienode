import 'jest'
import app from '../src'
import * as request from 'supertest'

describe('Integration tests', () => {
  it('can get hello message', async () => {
    const response = await request(app.callback()).get('/')
    expect(response.text).toBe('Hello')
  })

  it('can get paginated data', async () => {
    const response = await request(app.callback()).get('/movies')
    expect(response.body).toHaveProperty('page')
  })

  it('can get Avengers movie', async () => {
    const response = await request(app.callback()).get('/search?t=Avengers')
    expect(response.body.Title).toEqual('The Avengers')
  })

  it('can replace movie plot', async () => {
    const data = {
      title: 'The Avengers',
      find: 'heroes',
      replace: 'CLM Dev',
    }
    const response = await request(app.callback())
      .post('/replace-plot')
      .send(data)
    const isIncluded = response.text.includes('CLM Dev')
    expect(isIncluded).toBe(true)
  })
})
