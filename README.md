# MovieNode

Challenge for CLM Digital Solutions build on TypeScript

## Setup develop

```
cp .env.example .env
npm i
npm run dev
```

## Setup production

```
npm run production
```

## Usage

| Endpoint           | Description                                                         | Params                                          |
| ------------------ | ------------------------------------------------------------------- | ----------------------------------------------- |
| GET /movies        | Returns paginated movies stored in the DB                           |                                                 |
| GET /search        | Returns a movie and store it in the DB if it doesn't already exists | t: Movie title y: Movie year                    |
| POST /replace-plot | Returns movies's plot modified by given string                      | `{ title: string, find:string,replace:string }` |

## Documentation

[SwaggerHub](https://app.swaggerhub.com/apis/OscarERH/MovieNode/1.0.0)

## Test

`npm run test`

## Todo

- Dockerize project
- Add test database
- ~~Swagger documentation~~

## License

[WTFPL](http://www.wtfpl.net/about/)
