import koa from 'koa'
import bodyparser = require('koa-bodyparser')
import loggerKoa = require('koa-logger')
import cors = require('koa2-cors')
import dotenv = require('dotenv')
import mount = require('koa-mount')
import router from './routes'
import run from './database'

dotenv.config()
if (!process.env.PORT) {
  process.exit(1)
}
const app = new koa()

const PORT: number = parseInt(process.env.PORT as string, 10)

const URI = process.env.MONGO_URI || ''

run(URI).catch((err) => console.log(err))

app.use(loggerKoa())
app.use(cors())
app.use(bodyparser())

app.use(mount(router.routes()))

app.listen(PORT)

export default app
