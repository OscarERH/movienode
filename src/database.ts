import { connect } from 'mongoose'

export default async function run(URI): Promise<void> {
  await connect(URI)
}
