import Router = require('koa-router')
import { getMovies, sayHello } from '../controllers'
import {
  findAndReplacePlotValidated,
  searchMovieValidated,
} from '../validations'

const router = new Router()

router.get('/', sayHello)
router.get('/search', (ctx) => searchMovieValidated(ctx))
router.get('/movies', getMovies)
router.post('/replace-plot', (ctx) => findAndReplacePlotValidated(ctx))

export default router
