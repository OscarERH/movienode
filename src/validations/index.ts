import * as Joi from 'joi'
import { ParameterizedContext } from 'koa'
import { findAndReplacePlot, searchMovie } from '../controllers'

export function findAndReplacePlotValidated(ctx: ParameterizedContext) {
  const schema = Joi.object({
    title: Joi.string().required(),
    find: Joi.string().required(),
    replace: Joi.string().required(),
  })
  const { error } = schema.validate(ctx.request.body)
  if (error) {
    ctx.status = 400
    ctx.body = error.message
    return
  }
  return findAndReplacePlot(ctx)
}

export function searchMovieValidated(ctx: ParameterizedContext) {
  const schema = Joi.object({
    t: Joi.string().required(),
  })

  const { error } = schema.validate(ctx.query)
  if (error) {
    ctx.status = 400
    ctx.body = error.message
    return
  }
  return searchMovie(ctx)
}
