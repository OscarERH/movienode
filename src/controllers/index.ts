import { ParameterizedContext } from 'koa'
import {
  findAndReplacePlotService,
  searchMovieOnDatabaseService,
  searchMovieService,
} from '../services'

export async function sayHello(ctx: ParameterizedContext): Promise<void> {
  try {
    ctx.body = 'Hello'
    ctx.status = 200
  } catch (error) {
    ctx.body = 'Error'
    ctx.status = 500
  }
}

export async function searchMovie(ctx: ParameterizedContext): Promise<void> {
  try {
    const data = await searchMovieService(ctx.query)
    ctx.body = data
    ctx.status = 200
  } catch (error) {
    ctx.body = 'Error'
    ctx.status = 500
  }
}

export async function getMovies(ctx: ParameterizedContext): Promise<void> {
  try {
    const data = await searchMovieOnDatabaseService(ctx.query)
    ctx.body = data
    ctx.status = 200
  } catch (error) {
    ctx.body = 'Error'
    ctx.status = 500
  }
}

export async function findAndReplacePlot(
  ctx: ParameterizedContext,
): Promise<void> {
  try {
    const data = await findAndReplacePlotService(ctx.request.body)
    ctx.body = data
    ctx.status = 200
  } catch (error) {
    console.log(error)
    return error
  }
}
