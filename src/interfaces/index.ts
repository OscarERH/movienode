export interface Movie {
  Title: string
  Year: string
  Released: string
  Genre: string
  Director: string
  Actors: string
  Plot: string
  Ratings: Rating[]
}

export interface Rating {
  Source: string
  Value: string
}

export interface MovieDocument extends Document, Movie {}
