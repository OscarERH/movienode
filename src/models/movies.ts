import { Schema, model, PaginateModel } from 'mongoose'
import { MovieDocument } from '../interfaces'
import paginate = require('mongoose-paginate-v2')

export const schema = new Schema<MovieDocument>({
  Title: String,
  Year: String,
  Released: String,
  Genre: String,
  Director: String,
  Actors: String,
  Plot: String,
  Ratings: [
    {
      Source: String,
      Value: String,
      _id: false,
    },
  ],
})

schema.plugin(paginate as any)

const MovieModel = model<MovieDocument, PaginateModel<MovieDocument>>(
  'Movie',
  schema,
  'movies',
)

export default MovieModel
