import axios from 'axios'
import { Movie, MovieDocument } from '../interfaces'
import MovieModel from '../models/movies'
import { PaginateResult } from 'mongoose'

export async function searchMovieService(params): Promise<Movie> {
  try {
    const url = process.env.OMB_URL

    const { data } = await axios.get<Movie>(url, {
      params,
    })
    if (!data.Title) {
      throw new Error('Movie not found')
    }
    const movie = await saveMovieService(data)
    return movie
  } catch (error) {
    console.log(error)
    return error
  }
}

export async function saveMovieService(data: Movie): Promise<Movie> {
  try {
    const movie = await MovieModel.findOne({ Title: data.Title })
    if (movie) {
      return movie
    }
    const newMovie = MovieModel.create(data)
    return newMovie
  } catch (error) {
    console.log(error)
    return error
  }
}

export async function searchMovieOnDatabaseService(
  params,
): Promise<PaginateResult<MovieDocument>> {
  try {
    const movies = await MovieModel.paginate(
      {},
      { pagination: true, limit: 5, page: params.page || 1 },
    )
    return movies
  } catch (error) {
    console.log(error)
    return error
  }
}

export async function findAndReplacePlotService(data): Promise<string> {
  try {
    const movie = await MovieModel.findOne({ Title: data.title })
    if (!movie) {
      throw new Error('movie not found!')
    }
    const newPlot = replaceAll(movie.Plot, data.find, data.replace)
    return newPlot
  } catch (error) {
    console.log(error)
    return error
  }
}

function replaceAll(text, find, replace) {
  return text.split(find).join(replace)
}
